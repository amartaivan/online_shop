
var x = document.getElementById("number");

function sum(x) {
    if (x == 0) 
        return 0;
    return x + sum(x - 1);
}

function solve_sum(){
    var res = sum(parseInt(x.value));
    document.getElementById("demo").innerHTML = res;
}

function factorial(x) {
    if (x == 0) 
        return 1;
    return x * factorial(x - 1);
}

function solve_fac(){
    var res = factorial(parseInt(x.value));
    document.getElementById("demo").innerHTML = res;
}

function digit_sum(x) {
    if (x == 0)
        return 0;
    return  x% 10 + digit_sum(parseInt(x / 10));
}

function solve_dig(){
    var res = digit_sum(parseInt(x.value));
    document.getElementById("demo").innerHTML = res;
}

function reverse(x) {
    if (x == 1) 
        return '1';
    return x + ' ' + reverse(x - 1); 
}

function solve_rev(){
    var res = reverse(parseInt(x.value));
    document.getElementById("demo").innerHTML = res;
}

function print(x) {
    if (1 == x) 
        return '1';  
    return print(x - 1) + ' ' + x; 
}

function solve_pri(){
    var res = print(parseInt(x.value));
    document.getElementById("demo").innerHTML = res;
}

var memo = {};

function fibonacci(x) {
    if (x < 2)
        return x;
    if (x in memo) {
        return memo[x];
    }
    var ans =  fibonacci(x - 1) + fibonacci(x - 2);
    memo[x] = ans;
    return ans;
    }


function solve_fib(){
    var res = fibonacci(parseInt(x.value));
    document.getElementById("demo").innerHTML = res;
}

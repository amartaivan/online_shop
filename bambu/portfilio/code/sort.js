
var x = document.getElementById("number");

function myFunction() {
  document.getElementById("myDropdown").classList.toggle("show");
}

window.onclick = function(event) {
    if (!event.target.matches('.dropbtn')) {
        var dropdowns = document.getElementsByClassName("dropdown-content");
        var i;
        for (i = 0; i < dropdowns.length; i++) {
            var openDropdown = dropdowns[i];
            if (openDropdown.classList.contains('show')) {
                openDropdown.classList.remove('show');
            }
        }
    }
}

let bubbleSort = (x) => {
    let len = x.length;
    for (let i = 0; i < len; i++) {
        for (let j = 0; j < len; j++) {
            if (x[j] > x[j + 1]) {
                let tmp = x[j];
                x[j] = x[j + 1];
                x[j + 1] = tmp;
            }
        }
    }
    console.log(x)
    return x;
};

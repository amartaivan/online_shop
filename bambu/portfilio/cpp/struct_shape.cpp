#include<iostream>
#include<cmath>

using namespace std;

struct Shape
{
    int a, b, c;
    double area, p;

    void circle() {
        area = 3.14 * a * a;
    }

    void square() {
        area = a * b;
    }

    void triangle() {
        p =(a + b + c) / 2;
        area = sqrt(p * (p - a) * (p - b) * (p - c));
    }

    void printArea() {
        cout << area << endl;
    }
};

int main() {
    int n, input;
    cin >> n;
    int a, b, c;
    Shape sh;
    for(int i = 0; i < n; i++) {
        cin >> input;
        if(input == 1) {
            // toirog
            cin >> sh.a;
        } else {
            if(input == 2) {
                // tegsh untusgt
                cin >> sh.a >> sh.b;
            } else {
                //gurvaljin
                cin >> sh.a >> sh.b >> sh.c;
            }
        }
    }

    int ans;
    cin >> ans;
    switch (ans)
    {
        case 1:
            sh.circle();
            sh.printArea();
            break;
        case 2:
            sh.square();
            sh.printArea();
            break;
        case 3:
            sh.triangle();
            sh.printArea();
            break;
    }
}
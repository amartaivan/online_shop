#include<iostream>

using namespace std;

int main() {
    int numerator, denominator;
    cin >> numerator >> denominator;
    if(numerator >= 0 && denominator >= 0 && numerator / denominator >= 0) {
        cout << "Positive" << endl;
    } else {
        cout << "Negative" << endl;
    }
    return 0;
}
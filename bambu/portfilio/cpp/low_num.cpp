#include<iostream>

using namespace std;

int main() {

    int n, m, low = 10;
    cin >> n;
    while(n != 0) {
        m = n % 10;
        if(m <= low) {
            low = m;
        }
        n /= 10;
    }
    cout << low << endl;
    return 0;
}
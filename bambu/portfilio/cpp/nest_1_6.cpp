#include<iostream>
using namespace std;
int main() {
    int n, m ;
    cin >> n >> m ;
    if(n > 0) {
        if(m < 100) {
            for(int i=n ; i > 0 ; i-- ){
                for(int j=m ; j > 0 ; j-- ){
                    if ((i+j) % 2 == 0){
                        cout << "#";
                    } else {
                        cout << "_";
                    }
                    }
                cout << "\n";
                }
            }
        }
    return 0;
}
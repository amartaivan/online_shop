#include<iostream>

using namespace std;

int main() {

    int n, sum = 0, m;
    cin >> n;
    while(n != 0) {
        m = n % 10;
        if(m % 2 == 0) {
            sum += m;
        }
        n /= 10;
    }
    cout << sum << endl;
}
#include <iostream>
#include <fstream>

using namespace std;

int main () {
  ofstream myfile ("todo_list.txt");
  if (myfile.is_open())
  {
    int n = 0, cmd;
    string job[100];
    bool exit = false;

    while(exit == false) {
        cout << "press 0 for listing all jobs \n";
        cout << "press 1 for add new job \n";
        cout << "press 9 for exit \n";
        cin >> cmd;
        switch (cmd) {
            case 0 : {
                for (int i = 0; i < n; i++)
                    cout << i+1 << "." << job[i] << endl;
                break;
            }
            case 1 : {
                string new_job;
                cin >> new_job;
                job[n] = new_job;
                n++;
                break;
            }
            case 9 : {
                exit = true;
                break;
            }
        } 
        }
    } else cout << "Unable to open file";
return 0;
}
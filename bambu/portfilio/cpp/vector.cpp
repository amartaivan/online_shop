#include<iostream>
#include<string>
#include<fstream>
#include <stdlib.h> 
#include <time.h>

using namespace std;

int main() {
    freopen("test.txt", "r", stdin);
    srand (time(NULL));

    int n;
    cin >> n;
    int a[n];
    for (int i = 0; i < n; i++){
        cin >> a[i];
    }

    int max_val = a[0];
    for(int i = 1; i < n; i++){
        max_val = max(max_val, a[i]);
    }

    cout << max_val << endl;
    return 0;
}
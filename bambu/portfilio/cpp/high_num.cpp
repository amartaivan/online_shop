#include<iostream>

using namespace std;

int main() {

    int n, m, high = 0;
    cin >> n;
    while(n != 0) {
        m = n % 10;
        if(m >= high) {
            high = m;
        }
        n /= 10;
    }
    cout << high << endl;
    return 0;
}
#include<iostream>

using namespace std;

int main() {

    int n, num = 0, m, a;
    cin >> n;
    cin >> a;
    while(n != 0) {
        m = n % 10;
        if(m == a) {
            num += 1;
        }
        n /= 10;
    }
    cout << num << endl;
}
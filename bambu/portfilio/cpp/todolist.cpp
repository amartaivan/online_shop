#include<iostream>
#include<string>
#include<fstream>

using namespace std;

int main() {

    ifstream in("todo.txt");

    string job;
    string job_list[100];

    int n = 0;
    while(getline(in, job)){
        job_list[n] = job;
        n++;
    }
    bool exit = false;
    int cmd;

    while(exit == false) {
        cout << "press 0 for listing all jobs \n";
        cout << "press 1 for add new job \n";
        cout << "press 9 for exit \n";
        cin >> cmd;
        switch (cmd) {
            case 0 : {
                for (int i = 0; i < n; i++)
                    cout << i+1 << "." << job_list[i] << endl;
                break;
            }
            case 1 : {
                string new_job;
                cin >> new_job;
                job_list[n] = new_job;
                n++;
                ofstream out("todo.txt");
                for(int i = 0; i < n; i++){
                out << job_list[i] << endl;
                }
                break;
            }
            case 9 : {
                exit = true;
                break;
            }
        }   
    }
    return 0;
}

#include<iostream>
#include<vector>

using namespace std;

struct Point 
{
    int x, y;

    void up() {
        y++;
    }
    
    void down() {
        y--;
    }

    void left() {
        x--;
    }

    void right() {
        x++;
    }
};

int main() {
    vector<Point> p(2);
    cin >> p[0].x >> p[0].y;
    cin >> p[1].x >> p[1].y;
    int n;
    cin >> n;
    char f;
    int c;
    for(int i = 1; i<=n; i++) {
        cin >> c >> f;
        c--;
        switch (f)
        {
        case 'u':
        p[c].up();
            break;
        case 'd':
            p[c].down();
            break;
        case 'r':
            p[c].right();
            break;
        case 'l':
            p[c].left();
            break;
        }
    } 
    cout << p[0].x << " " << p[0].y << endl;
    cout << p[1].x << " " << p[1].y << endl;
}
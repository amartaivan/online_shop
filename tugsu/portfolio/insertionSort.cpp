#include<iostream>
#include<fstream>
#include<vector>
#include<ctime>

using namespace std;

vector<int> insertion_sort(vector<int>arr){
     
    int n = arr.size();

    for(int split = 0; split < n - 1; split++){

        int head = arr[split + 1];
        int target = 0;
        while (target <= split && arr[target] < head){
            target++;
        }
        for(int j = split; j >= target; j--){
            arr[j + 1] = arr[j];
        }
        arr[target] = head;
    }
    return arr;
}

int main(){

    ifstream in("text.txt");
    int n;
    in >> n;
    vector<int> arr(n), sorted_by_insertion;

    for(int i = 0; i < n; i++)
        in >> arr[i];
        
    clock_t begin, end;

    begin = clock();
    sorted_by_insertion = insertion_sort(arr);
    end = clock();
    ofstream insert_out("insert_out");
    insert_out << sorted_by_insertion.size() <<endl;
    
    for(int i = 0; i < sorted_by_insertion.size(); i++)
        insert_out << sorted_by_insertion[i] << " ";
    insert_out <<endl;

    cout<<"insertion sort time:" << double(end - begin) / 1000000 <<endl;
    
    

    return 0;
}
#include<iostream>
#include<fstream>
#include<vector>
#include<ctime>

using namespace std;

vector<int> selection_sort(vector<int> vecsort){
    int r = vecsort.size();;
    int heden_udaa = vecsort.size();;
    
    for(int i = 0; i < heden_udaa; i++) {
        int big_index = 0;
        for (int j = 0; j < r; j++)    {
            if (vecsort[j] > vecsort[big_index]){
                big_index = j;
            }
        }
        r--;
        swap(vecsort[big_index], vecsort[r]);
    }

    return vecsort;
}
vector<int> bubble_sort(vector<int> vec){
    int n = vec.size();;

    while(true) {
        bool swapped = false;
        for(int i = 0; i < n - 1; i++) {
            if(vec[i] > vec[i + 1]) {
                swapped = true;
                swap(vec[i], vec[i + 1]);
            }
        }
        if(!swapped) {
            break;
        }
    }
    return vec;
}

int main() {

    clock_t begin, end;
    freopen ("test.txt", "w", stdout);
    srand(time(NULL));
    int a;
    cin >> a;
    cout << a << endl;
    for(int i = 0; i < a; i++){
        int num = rand() % 100 + 1;
        cout << num << " ";
    }
    freopen("test.txt", "r", stdin);

    int n;
    cin >> n;
    vector<int> vecsort(n), sorted_by_select, sorted_by_bubble;
    for(int i = 0; i < n; i++) {
        cin >> vecsort[i];
    }

    begin = clock();
    sorted_by_select = selection_sort(vecsort);
    end = clock();
    ofstream select_out("selection.out");

    for(int i = 0; i < n; i++) 
        select_out << sorted_by_select[i] << " ";
    select_out << endl;
    cout << "selection_sort time :  " << double(end - begin) / 1000000 << endl;

    for(int i = 0; i < n; i++){
        cin >> vecsort[i];
    }

    begin = clock();
    sorted_by_bubble = bubble_sort(vecsort);
    end = clock();
    ofstream bubble_out("bubble.out");

    for(int i = 0; i < n; i++)
        bubble_out << sorted_by_bubble[i] << " ";
    bubble_out << endl;
    cout << "bubble_sort time :  " << double(end - begin) / 1000000 << endl;


    return 0;
}

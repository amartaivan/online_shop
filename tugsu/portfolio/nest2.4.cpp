#include <iostream>
using namespace std;

int main()
{
	int n, i, j, space;
	cin >> n;
	
	for(i = 1; i <= n; i++){
        for(space = i; space < n; space++){
            cout << "-";
        }
            for(j = 1; j <= (2 * i - 1); j++){
                cout << "*";
            }
            for(space = i; space <n; space++){
                cout << "-";
            }
            cout << endl;
    }
    return 0;
}
#include<iostream>
#include<math.h>

using namespace std;

int main(){
	int n, i, j, k;
	cin >> n;
    int a[n];

	for(i = 0; i < n; i++){
		cin >> a[i];
	}
	for(i = 0; i < n; i+=2){
        if (i != 0)
            cout << " ";
        cout << a[i];
	}
    cout << endl;

    for(i = 1; i < n; i+=2){
        if (i != 1)
            cout << " ";
        cout << a[i];
    }
    cout << endl;
    return 0;
}

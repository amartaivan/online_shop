#include<iostream>
#include<cmath>

using namespace std;

int main(){

    int n, max_digit;
    cin >> n;

    max_digit = n % 10;
    while (n > 0){
        max_digit = max(max_digit, n % 10);
        n = n / 10;
    }
    cout << max_digit << endl;
    return 0;
}
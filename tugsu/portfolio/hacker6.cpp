#include<iostream>
#include<math.h>

using namespace std;

int main(){
	int n, m, i, j;
	cin >> n >> m; 
    char a = '#';
    char b = '_';
    if((n + m) % 2 == 1) {
        a = '_';
        b = '#';
    }
    for (i = 1; i <= n; i++){
        for (j = 1; j <= m; j++){
            if((i + j) % 2 == 0){
                cout << a;
            }else{
                cout << b;
            }
        }
        cout << endl;
    }
    return 0;
}

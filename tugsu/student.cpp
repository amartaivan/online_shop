#include<iostream>
#include<string>
#include<fstream>
#include<vector>


using namespace std;


struct Exam {
    int exam1;
    int exam2;
    int final_exam;

    Exam(int _exam1, int _exam2, int _final_exam){
        exam1 = _exam1;
        exam2 = _exam2;
        final_exam = _final_exam;
    }

};
struct Student {
    int ID;
    string name;
    int age;
    string sex;
    int grade;
    string address;
    Exam ex;

    Student(string _name, int _age, string _sex, int _grade, string _address, Exam ex){
        name = _name;
        age = _age;
        sex = _sex;
        grade = _grade;
        address = _address;
    }    
};

int main(){
    string name, sex, address;
    int ID, age, grade, exam1, exam2, final_exam;
    vector<Student>student_list;
    int cmd = -1;
    while(cmd != 0){
        cout << "1: add new student \n";
        cout << "2: edit student info \n";
        cout << "3: list all students \n";
        cout << "4: enter exam score \n";
        cout << "5: show student detail \n";
        cin >> cmd;
        switch(cmd){
            case 1: {
                ID = 1; 
                cout << "Enter firstname && lastname: ";
                cin >> name;
                cout << "Enter age: ";
                cin >> age;
                cout << "Enter sex: ";
                cin >> sex;
                cout << "Enter grade: ";
                cin >> grade;
                cout << "Enter address: ";
                cin >> address;
                Student s = Student(name, age, sex, grade, address);
                student_list.push_back(s);
                ID++;
            break;
            }
            case 3: {
                for (int i = 0; i < student_list.size(); i++)
                {
                    cout << i + 1  << "." << student_list[i].name << endl;
                }   
            break;
            }
            case 4: {
                cout << "Enter exam1 score: ";
                cin >> exam1;
                cout << "Enter exam2 score: ";
                cin >> exam2;
                cout << "Enter final_exam score: ";
                cin >> final_exam;
                Exam e = Exam(exam1, exam2, final_exam);
                student_list.push_back(e);
            break;
            }
        }
    }
    return 0;
}
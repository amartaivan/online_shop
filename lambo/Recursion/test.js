const sumButton = document.querySelector("#sumBtn");
const sumNumButton = document.querySelector("#sumNumBtn");
const factButton = document.querySelector("#factBtn");
const fiboButton = document.querySelector("#fiboBtn");
const textArea = document.querySelector("#textArea");
const outcome = document.querySelector("#outcome");
const toN = document.querySelector("#ton");
const to1 = document.querySelector("#to1");

var sum = function(x) {
    if(x == 1)
        return 1;
    return x + sum(x - 1);
}

var fact = function(x) {
    if(x == 1)
        return 1;
    return (x * fact(x - 1)) % 2019;
}

var numeralsum = function(x) {
    if(x < 10)
        return parseInt(x);
    return x % 10 + numeralsum(parseInt(x / 10));
}

var reverse = function(x) {
    if(x == 1) 
        return '1';
    return x + ' ' + reverse(x - 1);
}

var oneToN = function(x, y) {
    if(y >= x) 
        return x;
    return y + ' ' + oneToN(x, y + 1);
}

<<<<<<< Updated upstream
var fib = {};
fib[0] = 0;
fib[1] = 1;
var fibo = function(x) {
    if(fib.hasOwnProperty(x)) 
        return  fib[x];
    fib[x] = fibo(x - 1) + fibo(x - 2);
    return fib[x];
=======
var fibo = function(x) {
    if(x == 0)
        return 0;
    if(x == 1)
        return 1; 
    return  fibo(x - 1) + fibo(x - 2);
>>>>>>> Stashed changes
}

factButton.addEventListener("click", function() {
    outcome.innerHTML = fact(parseInt(textArea.value));
})

sumButton.addEventListener("click", function() {
    outcome.innerHTML = sum(parseInt(textArea.value));
})

sumNumButton.addEventListener("click", function() {
    outcome.innerHTML = numeralsum(parseInt(textArea.value));
})

to1.addEventListener("click", function() {
    outcome.innerHTML = reverse(parseInt(textArea.value));
})

toN.addEventListener("click", function() {
    outcome.innerHTML = oneToN(parseInt(textArea.value),1);
})

fiboButton.addEventListener("click", function() {
    outcome.innerHTML = fibo(parseInt(textArea.value),1);
})

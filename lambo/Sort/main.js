// // Model Controller
// var sortController = (function() {

    
// })();

// // View Contoller
// var UIController = (function() {
//     var DOMstrings = {
//         inputType: '.add__type'
//     };
    
// })();

// // Global Controller
// var controller = (function(sortCtrl, UICtrl) {
        
// })(sortController, UIController);

// controller.init();

var startTime, endTime;

function start() {
  startTime = new Date();
};

function end() {
  endTime = new Date();
  var timeDiff = endTime - startTime; //in ms
  document.getElementById("content").innerHTML += " : " + timeDiff + " ms.";
}

function insertionSort(arr) {
	const n = arr.length;
	
	for(let i = 0; i < n - 1; i++) {
		let break_point = i + 1;
	    let pos_index = 0;
		while((pos_index < break_point) && (arr[pos_index] < arr[break_point])) {
			pos_index++;
		}
		let temp = arr[break_point];

		for(let j = break_point; j > pos_index; j--){

			arr[j] = arr[j - 1];
		}
		arr[pos_index] = temp;
	}
	return arr; 
};

function selectionSort(arr) {
	let n = arr.length;
	let index;

	while(n > 0) {
		index = 0;
		for(let i = 0; i < n; i++) {
			if(arr[i] > arr[index])
				index = i;
		}
        let temp = arr[index];
        arr[index] = arr[n - 1];
        arr[n - 1] = temp;
		n--;
	}
	return arr;  
};

function bubbleSort(arr) {
    let n = arr.length;

    while(n > 0) {
        let swapped = false;
        for(let j = 0; j < n - 1; j++) {
            if(arr[j] > arr[j + 1]) {
                swapped = true;
                let temp = arr[j];
                arr[j] = arr[j + 1];
                arr[j + 1] = temp;
            }
        }
        if(!swapped)
            break;
        n--;
    }
    return arr;
};

function stringToInt(arr) {
    for(let i = 0; i < arr.length; i++)
        arr[i] = parseInt(arr[i]);
    return arr;
};


var array = [];
var inputSelectionSort = [...array];
var inputBubbleSort = [...array];

document.getElementById("insertion__sort").addEventListener('click' , function() {
    document.getElementById("content").innerHTML = " ";
    array = document.getElementById("area").value.split(" ");
    var inputInsertionSort = [...array];
    insertionSort(stringToInt(inputInsertionSort));
    for(let i = 0; i < inputInsertionSort.length; i++) {
        if(!isNaN(inputInsertionSort[i]))
           document.getElementById("content").innerHTML += inputInsertionSort[i] + " ";
    }
})

document.getElementById("selection__sort").addEventListener('click' , function() {
    document.getElementById("content").innerHTML = " ";
    array = document.getElementById("area").value.split(" ");
    var inputSelectionSort = [...array];
    selectionSort(stringToInt(inputSelectionSort));
    for(let i = 0; i < inputSelectionSort.length; i++) {
        if(!isNaN(inputSelectionSort[i]))
            document.getElementById("content").innerHTML += inputSelectionSort[i] + " ";
    }
})

document.getElementById("bubble__sort").addEventListener('click' , function() {
    document.getElementById("content").innerHTML = " ";
    array = document.getElementById("area").value.split(" ");
    var inputBubbleSort = [...array];
    bubbleSort(stringToInt(inputBubbleSort));
    for(let i = 0; i < inputBubbleSort.length; i++) {
        if(!isNaN(inputBubbleSort[i]))
            document.getElementById("content").innerHTML += inputBubbleSort[i] + " ";
    }
})
  // Your web app's Firebase configuration
  var firebaseConfig = {
    apiKey: "AIzaSyDkQ2XLzqVY8zFRoZ5vTu1oj9EHw5nB7bo",
    authDomain: "angular-test-deploy-26a8d.firebaseapp.com",
    databaseURL: "https://angular-test-deploy-26a8d.firebaseio.com",
    projectId: "angular-test-deploy-26a8d",
    storageBucket: "angular-test-deploy-26a8d.appspot.com",
    messagingSenderId: "102825653083",
    appId: "1:102825653083:web:65ae1abdca84296b"
  };
  // Initialize Firebase
  firebase.initializeApp(firebaseConfig);
  const db = firebase.firestore();
  var provider = new firebase.auth.FacebookAuthProvider();
  const signI = document.querySelector("#sigin");
  const signO = document.querySelector("#sigout");
  const docRef = db.collection("Data").doc("btnData");
  const mesRef = db.collection("Chat").doc("msg");
  const blueBtn = document.querySelector("#blueBtn");
  const redBtn = document.querySelector("#redBtn");
  const nameArea = document.querySelector(".nameArea");
  const textArea = document.querySelector("#textArea");
  const sendBtn = document.querySelector("#sendBtn");
  const messageArea = document.querySelector("#messageArea");
  const mesInput = document.querySelector("#messageInput");
  var area51 = document.getElementById("area51");
  var are52 = document.getElementById("area52");
  var blueSize = 50;
  var redSize = 50;
  var speed = 0.25;
  var name;

  document.addEventListener("DOMContentLoaded", () => {
    docRef.get().then(function (doc) {
      if(doc && doc.exists) {
        const MyData = doc.data();
        area51.style.width = MyData.blue + '%';
        area52.style.width = MyData.red + '%';
        blueSize = MyData.blue;
        redSize = MyData.red;
      }
    })
});

  blueBtn.addEventListener("click", function() {
      blueSize -= speed;
      redSize = 100 - blueSize;
      area51.style.width = blueSize + '%';
      area52.style.width = redSize + '%';
      docRef.set({
          blue: blueSize ,
          red: redSize
          });
  })

  redBtn.addEventListener("click", function() {
      blueSize += speed;
      redSize = 100 - blueSize;
      area51.style.width = blueSize + '%';
      area52.style.width = redSize + '%';
      docRef.set({
          blue: blueSize ,
          red: redSize
          });
  })

  getRealTimeUpdatesKappa = function() {
    docRef.onSnapshot(function (doc) {
      if(doc && doc.exists) {
          const MyData = doc.data();
          area51.style.width = MyData.blue + '%';
          area52.style.width = MyData.red + '%';
          blueSize = MyData.blue;
          redSize = MyData.red;
        }
    });
  }
  getRealTimeUpdatesKappa();

  signI.addEventListener("click", function() {
    firebase.auth().signInWithPopup(provider).then(function(result) {
      // This gives you a Facebook Access Token. You can use it to access the Facebook API.
      var token = result.credential.accessToken;
      // The signed-in user info.
      var user = result.user;
      console.log(user,token);
    }).catch(function(error) {
      // Handle Errors here.
      var errorCode = error.code;
      var errorMessage = error.message;
      // The email of the user's account used.
      var email = error.email;
      // The firebase.auth.AuthCredential type that was used.
      var credential = error.credential;
      // ...
      });
  })

  signO.addEventListener("click", function() {
    firebase.auth().signOut().then(function() {
      console.log('Signout successful!')}, function(error) {
      console.log('Signout failed')
    });
  })

  firebase.auth().onAuthStateChanged(firebaseUser => {
    if(firebaseUser) {
      const html = `
      <h3>Welcome, ${firebaseUser.displayName}</h3>
      `;
      name = firebaseUser.displayName;
      nameArea.innerHTML = html;
      mesInput.style.display = "inline"
      signI.classList.add('hide');
      signO.classList.remove('hide');
      blueBtn.classList.remove('hide');
      redBtn.classList.remove('hide');
    } else {
      mesInput.style.display = "none"
      nameArea.innerHTML = '';
      blueBtn.classList.add('hide');
      redBtn.classList.add('hide');
      signO.classList.add('hide');
      signI.classList.remove('hide');
    }
  })

  textArea.addEventListener('keydown', function (e){
        if(e.which == 13){
          var comment;
          comment = name + ": " + textArea.value; 
          mesRef.set({
            value: comment 
            });
            var objDiv = document.getElementById("messageArea");
            objDiv.scrollTop = objDiv.scrollHeight;
        }
  }, false);

  sendBtn.addEventListener("click", function() {
    var comment;
    comment = name + ": " + textArea.value; 
    mesRef.set({
      value: comment 
      });
      var objDiv = document.getElementById("messageArea");
      objDiv.scrollTop = objDiv.scrollHeight;
  })

  chatRealTimeUpdatesKappa = function() {
    mesRef.onSnapshot(function (doc) {
      if(doc && doc.exists) {
          const MyData = doc.data();
          const html = `
          <p> ${MyData.value} </p>
          `;
          messageArea.innerHTML += html;
          textArea.value = " ";
        }
    });
  }
  chatRealTimeUpdatesKappa();
var counter = 0;

function myFunction() {
  var moreText = document.getElementById("more");
  var btnText = document.getElementById("myBtn");
  var lessText = document.getElementById("less");

  if (moreText.style.display === "none") {
    btnText.innerHTML = "Hide Socials"; 
    moreText.style.display = "inline";
    lessText.style.display = "none";
  } else {
    btnText.innerHTML = "Display Socials"; 
    moreText.style.display = "none";
    lessText.style.display = "inline";
  }
}

function addFunction() {
  var text = document.getElementById("idea").value; 
  var li = "<li>" + text + "</li>";
  counter++;
  if(counter <= 5) {
    document.getElementById("list").innerHTML += li;
    document.getElementById("idea").value = " ";
  }
  else
    alert("Full");
}
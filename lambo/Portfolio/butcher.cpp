#include<iostream>
#include<vector>
#include<fstream>

using namespace std;

double price = 47;

bool brother(double total, double salepoint) {

	if (total <= salepoint)
		return true;
	return false;
}

double daughter(double amount) {
   double total = price * amount;

   if (brother(total, 200))
       total *= 0.8;
   else
	   total *= 0.9;

    return total;			
}

int main() {
	double  weektotal = 0, amount;
	string name;
	int ln, key = 10;
	vector<pair <string, double>  > customer;

	for(int i = 0; i < 7; i++) {
		customer.clear();
		string base(".out");
		double sum = 0;
		key = 10;

		switch(i) {
			case 0: cout << " --- It's Monday --- " << endl << endl; break;
			case 1: cout << " --- It's Tuesday --- " << endl << endl; break;
			case 2: cout << " --- It's Wednesday --- " << endl << endl; break;
			case 3: cout << " --- It's Thursday --- " << endl << endl; break;
			case 4: cout << " --- It's Friday --- " << endl << endl; break;
			case 5: cout << " --- It's Saturday --- " << endl << endl; break;
			case 6: cout << " --- It's Sunday --- " << endl << endl; break;
		}

		while(key != 5) {
			cout << "Press 1 to Enter new order" << endl;
			cout << "Press any other number to proceed to next day" << endl;
			cin >> key;
			switch(key) {
				case 1:
					cout << "Enter Customer Name " << endl;
					cin >> name;
					cout << "Enter Amount : " << endl;
					cin >> amount;
					customer.push_back(make_pair(name, daughter(amount)));
					sum += daughter(amount);

					cout << "Your Total : ";
					cout << daughter(amount) << endl;
					break;
				default:
					key = 5;
					break;
			}
		}
		if(customer.size() >= 5) {
			cout << "Lucky Number Between 1 and " << customer.size() << " " << endl;

			cin  >> ln;

			if(ln > 0 && ln <= customer.size()) {
				cout << customer[ln-1].first << " , Lucky Customer of Today has paid : " << customer[ln-1].second << endl;
				sum -= customer[ln-1].second;
				swap(customer[customer.size() - 1], customer[ln - 1]);
				customer.pop_back();
			}
			else {
				cout << "Wrong Entry" << endl;
			}
		}

		weektotal += sum;
		
		cout << endl << "Income of today : " << sum << endl << endl;

		ofstream output(to_string(i + 1) + base);
		for(int i = 0; i < customer.size(); i++) 
			output << customer[i].first << " " << customer[i].second <<  "$" << endl;
		output << "Day Total : " << sum << "$" << endl;
	}
	
	cout << "Week Total: " << weektotal << endl;

	return 0;
}
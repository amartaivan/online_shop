#include <iostream>
#include <vector>
#include <fstream>
using namespace std;

int main() {
	int key = 10;
	string job;
	vector <string> job_list;
	ifstream input_data("todolist.txt");
	ofstream out("todolist.txt", ios::app);


	while(getline(input_data, job)) {
		if(job.size() > 0)
			job_list.push_back(job);
	}

	while(key != 9) {
		cout << "press 0 for listing all jobs" << endl; 
		cout << "press 1 for add new job" << endl;
		cout << "press 2 for delete first entry" << endl;  
		cout << "press 9 for exit" << endl; 
		cin >> key;

		switch(key) {
			case 0:
				if(job_list.size() != 0) {
					for(int i = 0; i < job_list.size(); i++)
						cout << i + 1 << ". " << job_list[i] << endl;
				}
				else
					cout << "No To-Do List" << endl;
				break; 
			case 1:
				cin >> job;
				job_list.push_back(job);
				out << job << endl;
				break;
			case 2:
				if(job_list.size() != 0)
					job_list.erase(job_list.begin());
				else
					cout << "No To-Do list" << endl;
				break;
			case 9:
				return 0;
			default:
				if(key != 10)
					cout << "Wrong Entry" << endl;
				break;
		}
		cout << endl;
	}
	
	return 0;
}	
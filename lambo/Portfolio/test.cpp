#include <iostream>
#include <vector>
#include <cmath>

using namespace std;

int main() {
    vector <int> chocolates;
    int n, size;
    cin >> n;
    int a_pos = 0, b_pos = n - 1;

    for(int i = 0; i < n; i++) {
      cin >> size;
      chocolates.push_back(size);
    }

    while(a_pos != b_pos) {
      if(chocolates[a_pos] < chocolates[b_pos]) {
          chocolates[b_pos] -= chocolates[a_pos];
          a_pos++;
      }
      else if(chocolates[a_pos] > chocolates[b_pos]) {
        chocolates[a_pos] -= chocolates[b_pos];
        b_pos--;
      } 
      else {
              a_pos++;
              b_pos++;
      }
    }

	  return 0;
}
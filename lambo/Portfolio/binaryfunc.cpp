#include <iostream>
using namespace std;

int bintodec(string bin) {
    int dec = 0, p = 1, n = bin.length();
    while(n >= 0) {
        if(bin[n - 1] == '1')
            dec += p; 
        p = p << 1;
        n--;
    }

    return dec;
}

string dectobin(int number) {
    string bin;
    
    while(number > 0) {
	    bin +=  to_string(number % 2);
		number = number >> 1;
	}

    int n = bin.length(); 
  
    for (int i = 0; i < n / 2; i++) 
        swap(bin[i], bin[n - i - 1]); 
    
    return bin;
}

int main() {
    int n;

    cin >> n;

    cout << dectobin(n) << endl;
    cout << bintodec("101000") << endl;
    return 0;
}
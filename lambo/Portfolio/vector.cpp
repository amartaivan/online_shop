#include <iostream>
using namespace std;

int main() {
    int n;
    freopen("test.txt", "r", stdin);

    cin >> n;
    int number[n];
    bool checker = false;

    for(int i = 0; i < n; i++) {
        cin >> number[i];
        if(number[i] == 10) {
            cout << i + 2 << " ";
            checker = true;
        }
    }

    if(!checker)
        cout << "NO";

    for(int i = 0; i < n; i++) {
        cin >> number[i];
        if(number[i] == 10) {
            for(int j = i + 1; j < n; j++) {
                swap(number[j],number[j - 1]);
            }
            n--;
            i--;
        }
    }    

    freopen("test.txt", "w", stdout);

    cout << n << endl;

    for(int i = 0; i < n; i++)
        cout << number[i] << endl;

    return 0;
}
#include <iostream>
#include <vector>

using namespace std;

int _id = 0;

struct Exam {
    double exam1 = 0.0;
    double exam2 = 0.0;
    double finalExam = 0.0;

    double getAverage() {
        return (exam1 + exam2 + finalExam) / 3.0;
    }
};

struct Student
{
    string name, sex, address;
    int grade, age, id;
    Exam ex;
    
    Student(string _name, string _sex, string _address, int _age, int _grade) {
        name = _name;
        sex = _sex;
        address = _address;
        age = _age;
        grade = _grade;
        id = _id;
        _id++;
    }
};

int main() {
    bool uni = true;
    vector <Student> student_list; 
    string name, sex, address;
    int grade, age, code;
    int option = 10, suboption;

    while(option != 6) {
        cout << "(1) Add new student " << endl;
        cout << "(2) Edit student info " << endl;
        cout << "(3) List all students " << endl;
        cout << "(4) Enter exam score " << endl;
        cout << "(5) Show student detail " << endl;
        cout << "(6) Exit " << endl;
        cin >> option;

        switch(option) {
            case 1:
                cout << " Enter full name " << endl;
                // getline(cin, person.name);
                cin >> name;
                uni = true;
                for(int i = 0; i < student_list.size(); i++) {
                    if(student_list[i].name == name) {
                        cout << "Not unique name" << endl;
                        uni = false;
                    }
                }
                if(uni) {
                    cout << " Enter age " << endl;
                    cin >> age;
                    cout << " Enter sex " << endl;
                    cin >> sex;
                    cout << " Enter address " << endl;
                    cin >> address;
                    cout << " Enter grade " << endl;
                    cin >> grade;
                    Student person = Student(name, sex, address, age, grade);
                    student_list.push_back(person);
                }
                break;
            case 2:
                cout << "1. Edit student info by id" << endl;
                cout << "2. Edit student info by name" << endl;
                cin >> suboption;
                if(suboption == 1) {
                    cout << "Enter ID" << endl;
                    cin >> code;
                    for(int i = 0; i < student_list.size(); i++) {
                        if(student_list[i].id == code) {
                            cout << " Enter full name " << endl;
                            cin >> name;
                            uni = true;
                            for(int j = 0; j < student_list.size(); j++) {
                                if(student_list[j].name == name) {
                                    cout << "Not unique name" << endl;
                                    uni = false;
                                }
                            }
                            if(uni) {
                                student_list[i].name = name;
                                cout << " Enter age " << endl;
                                cin >> student_list[i].age;
                                cout << " Enter sex " << endl;
                                cin >> student_list[i].sex;
                                cout << " Enter address " << endl;
                                cin >> student_list[i].address;
                                cout << " Enter grade " << endl;
                                cin >> student_list[i].grade;
                                break;
                            }
                        }
                    }
                }
                else if (suboption == 2) {
                    cout << "Enter name" << endl;
                    cin >> name;
                    for(int i = 0; i < student_list.size(); i++) {
                        if(student_list[i].name == name) {
                            cout << " Enter full name " << endl;
                            cin >> name;
                            uni = true;
                            for(int j = 0; j < student_list.size(); j++) {
                                if(student_list[j].name == name) {
                                    cout << "Not unique name" << endl;
                                    uni = false;
                                }
                            }
                            if(uni) {
                                student_list[i].name = name;
                                cout << " Enter age " << endl;
                                cin >> student_list[i].age;
                                cout << " Enter sex " << endl;
                                cin >> student_list[i].sex;
                                cout << " Enter address " << endl;
                                cin >> student_list[i].address;
                                cout << " Enter grade " << endl;
                                cin >> student_list[i].grade;
                                break;
                            }
                        }
                    }
                }
                break;
            case 3:
                for(int i = 0; i < student_list.size(); i++) {
                    cout << student_list[i].id << " | " << student_list[i].name 
                    << " | " << student_list[i].age 
                    << " | " << student_list[i].sex << " | " << student_list[i].grade << " | " << student_list[i].address << " | ";
                    cout << student_list[i].ex.exam1 << " | " << student_list[i].ex.exam2 << " | " << student_list[i].ex.finalExam << endl; 
                }
                break;
            case 5:
                cout << "1. Edit student info by id" << endl;
                cout << "2. Edit student info by name" << endl;
                cin >> suboption;
                if(suboption == 1) {
                    cout << "Enter ID" << endl;
                    cin >> code;
                    for(int i = 0; i < student_list.size(); i++) {
                        if(student_list[i].id == code) {
                            cout << student_list[i].name 
                            << " | " << student_list[i].age 
                            << " | " << student_list[i].sex << " | " << student_list[i].grade << " | " << student_list[i].address << " | ";
                            cout << student_list[i].ex.exam1 << " | " << student_list[i].ex.exam2 << " | " << student_list[i].ex.finalExam << 
                            " | Average Test Score is : " <<  student_list[i].ex.getAverage() << endl;
                            break; 
                        }
                    }  
                }
                else if(suboption == 2) {
                    cout << "Enter name" << endl;
                    cin >> name;
                    for(int i = 0; i < student_list.size(); i++) {
                        if(student_list[i].name == name) {
                            cout << student_list[i].name 
                            << " | " << student_list[i].age 
                            << " | " << student_list[i].sex << " | " << student_list[i].grade << " | " << student_list[i].address << " | ";
                            cout << student_list[i].ex.exam1 << " | " << student_list[i].ex.exam2 << " | " << student_list[i].ex.finalExam << 
                            " | Average Test Score is : " <<  student_list[i].ex.getAverage() << endl;
                            break; 
                        }
                    }                 
                }
                break;
            case 4:
                cout << "1. Edit student info by id" << endl;
                cout << "2. Edit student info by name" << endl;
                cin >> suboption;
                if(suboption == 1) {
                    cout << "Enter id" << endl;
                    cin >> code;
                    for(int i = 0; i < student_list.size(); i++) {
                        if(student_list[i].id == code) {
                            cout << "Enter score of exam 1" << endl;
                            cin >> student_list[i].ex.exam1;
                            cout << "Enter score of exam 2" << endl;
                            cin >> student_list[i].ex.exam2;
                            cout << "Enter score of final exam" << endl;
                            cin >> student_list[i].ex.finalExam;
                            break;
                        }
                    }
                }
                else if (suboption == 2) {
                    cout << "Enter name" << endl;
                    cin >> name;
                    for(int i = 0; i < student_list.size(); i++) {
                        if(student_list[i].name == name) {
                            cout << "Enter score of exam 1" << endl;
                            cin >> student_list[i].ex.exam1;
                            cout << "Enter score of exam 2" << endl;
                            cin >> student_list[i].ex.exam2;
                            cout << "Enter score of final exam" << endl;
                            cin >> student_list[i].ex.finalExam;
                            break;
                        }
                    }
                }
            default:
                break;
        }
    }
    return 0;
}
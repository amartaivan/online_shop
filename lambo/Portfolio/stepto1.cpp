#include<iostream>
#include<unistd.h>
using namespace std;
int main() {
	int N, k = 0;
	
	cin >> N;
	
	while(N != 1)
	{
		cout << "Step " << k + 1 << ":" << endl;
		if(N % 2 == 0)
		{
			N /= 2;
			cout << N << endl;
		}
		else
		{
			N = N * 3 + 1;
			cout << N << endl;
		}
		k++;
		sleep(2);
	}
	
	cout << k << " Steps" << endl;
		
	return 0;
}

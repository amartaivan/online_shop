#include <iostream>
using namespace std;

int main() {
	
	long a, n;
	
	cin >> a >> n;
	long p = a, ans = 1;

	while(n > 0){
		if (n % 2 == 1)
			ans = ans * p;
		p = p * p;
		n = n / 2;
	}

	cout << ans << endl;

	return 0;
}
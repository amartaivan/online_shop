#include<iostream>

using namespace std;

int main() {
    
    int n;
    cin >> n;
    int nums[n];

    for (int i = 0; i < n; i++)
        cin >> nums[i];
    
    int temp = nums[n - 1];

    for (int i = n - 1; i > 1; i--) {
        nums[i - 1] = nums[i - 2];
    }

    nums[0] = temp;

    for (int i = 0; i < n; i++)
        cout << nums[i] << " ";

    for (int i = 0; i < n; i++)
        cout << nums[i];

    cout << endl;
    
}
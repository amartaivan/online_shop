#include<iostream>
#include<ctime>
using namespace std;
int main() {
	int i, n = 100000000, sum;
	clock_t start = clock();
	for(int j = 0; j <= 9; j++) {
		sum = 0;
		i = 1;
		while(i <= n) {
			sum += i;
			sum = sum % 100;
			i++;
		}
	}
	cout << sum << endl;
	cout << ((float)(clock()-start))/CLOCKS_PER_SEC << " seconds." << endl;
}

#include<iostream>

using namespace std;

int main() {
    
    int n, mindex, k;
    bool checker = false;

    cout << "Size of array" << endl;
    cin >> n;

    while(checker == false) {
        cin >> k;
        if(k <= n)
            checker = true;
        else
            cout << "Wrong Entry! Try Again" << endl;
    }

    int nums[n];

    for (int i = 0; i < n; i++)
        cin >> nums[i];
    
    for(int i = 0; i < k; i++) {
        int mindex = i;
        for(int j = i + 1; j < n; j++) {
            if(nums[j] < nums[mindex]) 
                mindex = j;	
        }
        swap(nums[i], nums[mindex]);
    }

    cout << nums[k - 1] << endl;
    /* 
    for (int i = 0; i < n; i++)
        cout << nums[i] << " ";
    return 0;
    */
}
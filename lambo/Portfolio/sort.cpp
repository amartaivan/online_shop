#include <iostream>
#include <vector>
#include <ctime>
#include <fstream>
#include <unistd.h>
using namespace std;

vector <int> selection_sort(vector <int> unsorted_array) {
	int n = unsorted_array.size();
	int index;

	while(n > 0) {
		index = 0;
		for(int i = 0; i < n; i++) {
			if(unsorted_array[i] > unsorted_array[index])
				index = i;
		}
		swap(unsorted_array[index], unsorted_array[n - 1]);
		n--;
	}

	return unsorted_array;  //sorted array
}

vector <int> bubble_sort(vector <int> unsorted_array) {
	int n = unsorted_array.size();

	while(n > 0) {
		bool swapped = false;
		for(int j = 0; j < n - 1; j++) {
			if(unsorted_array[j] > unsorted_array[j + 1]) {
				swapped = true;
				swap(unsorted_array[j], unsorted_array[j + 1]);
			}
		}

		if(!swapped)
			break;
            
		n--;
	}

	return unsorted_array;  //sorted array	
}

vector <int> count_sort(vector <int> unsorted_array) {
	int n = unsorted_array.size();
	vector <int> count(1001,0);
	for(int i = 0; i < n; i++) {
		count[unsorted_array[i]]++;
	}
	int temp = 0, i = 0;
	while(n > 0) {
		while(count[temp] != 0) {
			unsorted_array[i] = temp;
			count[temp]--;
			n--;
			i++;
		}
		temp++;
	}

	return unsorted_array;  //sorted array	
}

vector <int> insertion_sort(vector <int> unsorted_array) {
	int n = unsorted_array.size();
	
	for(int i = 0; i < n - 1; i++) {
		int break_point = i + 1;
		int pos_index = 0;
		while((pos_index < break_point) && (unsorted_array[pos_index] < unsorted_array[break_point])) {
			pos_index++;
		}

		int temp = unsorted_array[break_point];

		for(int j = break_point; j > pos_index; j--){

			unsorted_array[j] = unsorted_array[j - 1];
		}
		unsorted_array[pos_index] = temp;
	}

	return unsorted_array;  //sorted array	
}

int main() {
	vector <int> nums, temp, temp1, temp2, temp3;
	int n, number;

	freopen("test.txt", "r", stdin);

	cin >> n;

	for(int i = 0; i < n; i++) {
		cin >> number;
		nums.push_back(number);
	}

	ofstream o_selection("selection_sort.txt"); //Selection Sort
	clock_t start = clock();
	temp3 = selection_sort(nums);

	for(int i = 0; i < temp3.size(); i++)
		o_selection << temp3[i] << " ";
	cout << endl << "Selection sort: " << double(clock() - start) / CLOCKS_PER_SEC  << " seconds!" << endl; // end of Selection Sort

	ofstream o_bubble("bubble_sort.txt"); // Bubble Sort
	start = clock();
	temp = bubble_sort(nums);

	for(int i = 0; i < temp.size(); i++)
		o_bubble << temp[i] << " ";
	cout << "Bubble sort: " << double(clock() - start) / CLOCKS_PER_SEC  << " seconds!"  << endl; // end of Bubble Sort

	ofstream o_count("count_sort.txt");  // Count sort
	start = clock();
	temp1 = count_sort(nums);

	for(int i = 0; i < temp1.size(); i++)
		o_count << temp1[i] << " ";
	cout << "Count sort: " << double(clock() - start) / CLOCKS_PER_SEC  << " seconds!" << endl; // end of Count Sort

	ofstream o_insertion("insertion_sort.txt");  // Insertion sort
	start = clock();
	temp2 = insertion_sort(nums);

	for(int i = 0; i < temp2.size(); i++)
		o_insertion << temp2[i] << " ";
	cout << "Insertion sort: " << double(clock() - start) / CLOCKS_PER_SEC  << " seconds!" << endl << endl; // end of Insertion Sort

	return 0;
}
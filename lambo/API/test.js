var token, id, title, description, body, en_title, en_description, en_body, email, password;

var loginUrl = "http://52.18.91.64/api/login"
var loginInfo = {
    "email": email,
    "password": password
};
var faqUrl = "http://52.18.91.64/api/faq";
var faqData = {
    "title": title,
    "description": description,
    "body": body,
    "en_title": "N/A",
    "en_description": "N/A",
    "en_body": "null"
}

var editData = {
    "id": id,
    "title": title,
    "description": description,
    "body": body,
    "en_title": "N/A",
    "en_description": "N/A",
    "en_body": "N/A"
}

const login = () => {
    loginInfo["email"] = document.getElementById('eInput').value;
    loginInfo["password"] = document.getElementById('pInput').value;
    document.getElementById('eInput').value = "";
    document.getElementById('pInput').value = "";
    fetch(loginUrl, {
    method: 'POST', 
    body: JSON.stringify(loginInfo), 
    headers:{
        'Content-Type': 'application/json',
    }
    }).then(res=> res.json())
    .then((response) => {
        console.log("ID: " + response.user.id);
        console.log("Name: " + response.user.name);
        console.log("Email: " + response.user.email);
        console.log("Type: " + response.user.type);
        console.log("Phone: " + response.user.phone);
        console.log("----------------------");
        console.log("Succesfully Logged In!");
        document.querySelector('#loginSection').style.display = 'none';
        document.querySelector('#loggedUser').style.display = 'inline';
        showList();
    })
    .catch(error => console.error('Error:', error));
}

const postFaq = () => {
    faqData["title"] = document.getElementById('title').value;
    faqData["description"] = document.getElementById('description').value;
    faqData["body"] = document.getElementById('body').value;
    document.getElementById('title').value = "";
    document.getElementById('description').value = "";
    document.getElementById('body').value = "";
    fetch(faqUrl, {
    method: 'POST', 
    body: JSON.stringify(faqData), 
    headers:{
        'Content-Type': 'application/json',
        "Authorization": `Bearer ${token}`
    }
    }).then(res=> res.json())
    .then((response) => {
        if(response.success === true)
            console.log("Added Succesfully");
        showList();
    })
    .catch(error => console.error('Error:', error));
}

const showList = () => {
    fetch(faqUrl, {
        method: 'GET', 
        headers:{
            'Content-Type': 'application/json',
        }
        }).then(res=> res.json())
        .then((response) => {
            showToUser(response);
            // document.getElementById('result').innerHTML = "";
            // var n = response.length;
            // for(let i = 0; i < n; i++) {
            //     document.getElementById('result').innerHTML += '<p>' + response[i].title + '</p>';
            // }
        })
        .catch(error => console.error('Error:', error));
}

const editList = () => {
    editData["id"] = parseInt(document.getElementById('id_input').value);
    editData["title"] = document.getElementById('title_2').value;
    editData["description"] = document.getElementById('description_2').value;
    editData["body"] = document.getElementById('body_2').value;
    document.getElementById('title_2').value = "";
    document.getElementById('description_2').value = "";
    document.getElementById('body_2').value = "";
    document.getElementById('id_input').value = "";
    fetch(faqUrl, {
        method: 'PUT', 
        body: JSON.stringify(editData), 
        headers:{
            'Content-Type': 'application/json',
            "Authorization": `Bearer ${token}`
        }
        }).then(res=> res.json())
        .then((response) => {
            console.log(response);
            showList();
        })
        .catch(error => console.error('Error:', error));   
}  

const showToUser = (content) => {
    let length = content.length;
    document.getElementById('result').innerHTML = "";
    for(let i = 0; i < length; i++) {
        html = `<div class="col s12 l4">
                        <div class="card">
                            <div class="card-image">
                              <img src="default.png" alt="">
                            </div>
                            <div class="card-content">
                                    <span class="card-title">${content[i].id}.  ${content[i].title}</span>
                                    <p>${content[i].description}</p>
                                    <p>${content[i].body}</p>
                            </div>
                        </div>
                </div>`;
        document.getElementById('result').innerHTML += html;
    }    
}

document.getElementById('subBtn').addEventListener('click' , login);
document.getElementById('faqBtn').addEventListener('click' , postFaq);
document.getElementById('editBtn').addEventListener('click' , editList);

// $(document).ready(function(){
//     $('.modal').modal();
//   });

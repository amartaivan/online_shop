#include <iostream>
using namespace std;

int main() {
    int a, hour, min, sec;
    cin >> a;
    hour = a / 3600;
    min = (a % 3600) / 60; 
    sec = a % 60;
    cout << hour << " " << min << " " << sec << endl;
    return 0;
}
#include <iostream>
using namespace std;

int main () {
    int a, digit, sum = 0;
    cin >> a ;
    while (a > 0){
        digit = a % 10;
        if (digit % 2 == 1){
            sum = sum + 1;
        }
        a = a / 10;
    } 
    cout << sum << endl;
    return 0;
}
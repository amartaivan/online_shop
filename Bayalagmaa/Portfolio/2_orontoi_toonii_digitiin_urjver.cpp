#include <iostream>
using namespace std;

int main() {
    int a, digit, multi = 1;
    cin >> a;
    digit = a % 10;
    multi = multi * digit;
    a = a / 10;
    digit = a % 10;
    multi = multi * digit;
    cout << multi << endl;
    return 0;
}
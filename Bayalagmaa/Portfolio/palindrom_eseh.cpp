#include <iostream>
using namespace std;

int main() {
    int a , digit, r = 0, b;
    cin >> a;
    b = a;
    while (a > 0 ){
        digit = a % 10;
        r = ( r * 10 ) + digit;
        a = a / 10;
    }
    if( b == r ){
        cout << "YES" << endl;
    } else {
        cout << "NO" << endl;
    }
    return 0;
}
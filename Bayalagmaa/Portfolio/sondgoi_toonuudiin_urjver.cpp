#include <iostream>
using namespace std;

int main() {
    int a, b, c, product = 1;
    cin >> a >> b >> c;
    if (a % 2 == 1) {
        product = product * a;
    }
    if (b % 2 == 1) {
        product = product * b;
    }
    if (c % 2 == 1) {
        product = product * c;
    }
cout << product << endl;
return 0;
}
#include <iostream>
using namespace std;

int main() {
    int a, digit, b = 0, c;
    cin >> a;
    c = a;
    while ( a > 0 ) {
        digit = a % 10;
        if( digit > b ){
            b = digit;
        }
        a = a / 10;
    }
    while (c > 0) {
        digit = c % 10;
        if ( b > digit ){
            b = digit;
        }
        c = c / 10;
    }
    cout << b << endl;
    return 0;
}
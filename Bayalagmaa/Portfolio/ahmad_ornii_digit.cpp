#include <iostream>
using namespace std;

int main() {
    int a, digit;
    cin >> a;
    while (a > 0) {
        digit = a % 10;
        a = a / 10;
    }
    cout << digit << endl;
    return 0;
}
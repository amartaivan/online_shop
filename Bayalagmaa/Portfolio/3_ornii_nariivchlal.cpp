#include <iostream>   
#include <iomanip>     //std::setprecision 
using namespace std;

int main() {
    int i = 1;
    double sum=0, input;
    int n;
    cin >> n;
    
    while (i <=n ) {
        cin >> input;
        sum = sum + input;
        i++;
    }
    
    sum=sum/n;
    std::cout << std::fixed << std::setprecision(3) << sum << endl;
    
}
#include <iostream>
using namespace std;

int main() {
    int a;
    cin >> a;
    if ( a > 89 ) {
        cout << "A" << endl;
    }
    if( 89 > a && a > 79 ) {
        cout << "B" << endl;
    }
    if ( 79 > a && a > 69) {
        cout << "C" << endl;
    }
    if ( 69 > a && a > 59 ) {
        cout << "D" << endl;
    }
    if ( 59 > a) {
        cout << "F" << endl;
    }
    return 0;
}
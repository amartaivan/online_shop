#include <iostream>
using namespace std;

int main() {
    int a, b, digit, sum = 0 ;
    cin >> a >> b;
    while (a > 0){
        digit = a % 10;
        if (digit == b) {
            sum = sum + 1;
        }
        a = a / 10;
    }
cout << sum << endl;
return 0;
}
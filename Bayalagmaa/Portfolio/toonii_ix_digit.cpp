#include <iostream>
using namespace std;

int main() {
    int a, digit, b = 0;
    cin >> a;
    while ( a > 0 ) {
        digit = a % 10;
        if( digit > b ){
            b = digit;
        }
        a = a / 10;
    }
    cout << b << endl;
    return 0;
}
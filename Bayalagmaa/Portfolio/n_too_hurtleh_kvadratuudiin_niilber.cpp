#include <iostream>
using namespace std;

int main() {
    int a, i = 1, sum = 0;
    cin >> a;
    while( i <= a ) {
        sum = sum + i * i;
        i++;
    }
    cout << sum << endl;
    return 0;
}
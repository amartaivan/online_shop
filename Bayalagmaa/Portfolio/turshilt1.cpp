#include <iostream>
using namespace std;

int main(){
    double kg, bill , price = 46, day_income, week_income;
    int day = 1;

    week_income = 0;
    
    while (day <= 7){
        int start = 1;
        day_income = 0;
        while(start <= 5){
            cout << "Hello! Welcome to my shop." << endl;
            cout << "1 kg meat is 46 mnt, How much would you like?" << endl;
            cin >> kg;
            bill = price * kg;
            if (bill >= 200){
                    bill = bill * 0.8;
            }else{
                    bill = bill * 0.9;
            }
            day_income = day_income + bill;
            cout << "Your bill is : " << bill << endl;
            start = start + 1;            
        }
        cout << "Today's bill is : " << day_income << endl;
        week_income = week_income + day_income;
        day = day + 1;
    }
    cout << "Week income is : " << week_income << endl;
    return 0;

}
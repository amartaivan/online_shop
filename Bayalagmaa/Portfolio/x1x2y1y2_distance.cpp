#include <iostream>
#include <cmath>
#include <iomanip>
using namespace std;

int main() {
    double x1, x2, y1, y2, n;
    cin >> x1 >> x2 >> y1 >> y2;
    n = sqrt((x2-x1)^2 + (y2-y1)^2);
    std::cout << std::setprecision(3) << n << endl;
    return 0
}
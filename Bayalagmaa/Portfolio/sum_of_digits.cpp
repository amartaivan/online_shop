#include <iostream>
using namespace std;

int main() {
    int a, sum = 0, digit;
    cin >> a;
    while ( a > 0 ) {
        digit = a % 10;
        sum = sum + digit;
        a = a / 10;
    }
    cout << sum << endl; 
    return 0;
}
#include <iostream>

using namespace std;

int main() {

    int n;
    cin >> n;
    int num[n];

    for (int i = 0; i < n; i++){
        cin >> num[i];
    }

    for (int i = 0; i < n; i+=2){
        cout << num[i] << " ";
    }
    cout << endl;

    for (int i = 1; i < n; i+=2){
        cout << num[i] << " ";
    }
    cout << endl;

    return 0;
}
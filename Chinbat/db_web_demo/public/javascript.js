// Your web app's Firebase configuration
var firebaseConfig = {
    apiKey: "AIzaSyBoE1Pg72fwdG632aKeW8h36pWA4Y_Bpxc",
    authDomain: "db-nest.firebaseapp.com",
    databaseURL: "https://db-nest.firebaseio.com",
    projectId: "db-nest",
    storageBucket: "db-nest.appspot.com",
    messagingSenderId: "810540769895",
    appId: "1:810540769895:web:1f60d96ac275c92e"
};

// Initialize Firebase
firebase.initializeApp(firebaseConfig);

let database = firebase.firestore();
let docref = database.collection('numbers').doc('BBBhaujzqVKnb98B3nxg')

let NUMBER = 0;
read_data();
listen_for_data_change();

function read_data() {
    docref.get().then(doc => {
        myNumber = doc.data()['number']
        console.log("read_data : ", myNumber)
        document.getElementById("number").value = myNumber;   
        NUMBER = myNumber;
    });
}

function update_data(x) {
    docref.set({
        number: x
      });
}

function listen_for_data_change (){
    
    var observer = docref.onSnapshot(
        docSnapshot => {

            // NUMBER = docSnapshot._document.proto.fields.number.integerValue;
            read_data();
            console.log('data change detected !', NUMBER);
            set_number();
        },
        err => {
            console.log(`data change listener encountered error: ${err}`);
        }
    );
}

var USER_DATA = null;

function facebookLogin () {
    var provider = new firebase.auth.FacebookAuthProvider();

    firebase.auth().signInWithPopup(provider).then(function(result) {
        // This gives you a Facebook Access Token. You can use it to access the Facebook API.
        var token = result.credential.accessToken;
        console.log('token :: ', token);
        // The signed-in user info.
        var user = result.user;

        console.log('user_data :: ', user.displayName);
        USER_DATA = user;
        set_name(user.displayName);
        change_visibility('login_button', 'hidden');
        change_visibility('logout_button', 'visible');
    }).catch(function(error) {
        // Handle Errors here.
        var errorCode = error.code;
        var errorMessage = error.message;
        // The email of the user's account used.
        var email = error.email;
        // The firebase.auth.AuthCredential type that was used.
        var credential = error.credential;
        // ...
    });  
}

firebase.auth().onAuthStateChanged(function(user) {
    if (user) {
        USER_DATA = user;
        set_name(user.displayName);
        change_visibility('login_button', 'hidden');
        change_visibility('logout_button', 'visible')
        change_visibility('add_button', 'visible');
        change_visibility('sub_button', 'visible');
        add_image('user_photo', 'visible', user.photoURL);
    } else {
        set_name('');
        change_visibility('login_button', 'visible');
        change_visibility('logout_button', 'hidden');
        change_visibility('add_button', 'hidden');
        change_visibility('sub_button', 'hidden');
        add_image('user_photo', 'hidden', '');
    }
});


function facebookLogout() {
    firebase.auth().signOut().then(function() {
        console.log("log out successfully!");
        set_name('');
        change_visibility('login_button', 'visible');
        change_visibility('logout_button', 'hidden');
      }).catch(function(error) {
        // An error happened.
      });
}


function set_name(name) {
    document.getElementById("name").innerHTML = name;   
}

function add_image(id, state, url) {
    document.getElementById(id).style.visibility = state;
    document.getElementById(id).src = url;
}

function change_visibility(id, state) {
    document.getElementById(id).style.visibility = state;  
}

function set_number() {
    document.getElementById("number").value = NUMBER;
}

function subClick() {
    console.log('NUMBER :: ', NUMBER);
    update_data(parseInt(NUMBER) - 1);
}


function addClick() {
    console.log('NUMBER :: ', NUMBER);
    update_data(parseInt(NUMBER) + 1);
}

function redClick() {
    document.getElementById("add_button").style.background='red';
    document.getElementById("sub_button").style.background='red';

}

function blueClick() {
    document.getElementById("add_button").style.background='blue';
    document.getElementById("sub_button").style.background='blue';
}




var texts = [];

function get_list_html() {

    ss = texts.length;
    text = "<ul>";
    for (i = 0; i < ss; i++) {
        text += "<li>" + texts[i] + "</li>";
    }
    text += "</ul>";
    return text;
}

function getText() {
    var x = document.getElementById("text_input").value;
    console.log(x);
    texts.push(x);
    if (texts.length > 5) {
        texts.reverse();
        texts.pop();
        texts.reverse();
    }
    document.getElementById("text_results").innerHTML = get_list_html();
}




// function get_user_data() {
//     var user = firebase.auth().currentUser;
//     if (user) {
//         console.log("USER :: ", user.displayName, user.email);
//     } else {
//         console.log("USER :: ", null);
//     }
// }
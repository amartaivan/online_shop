#include<iostream>
#include<vector>
#include<utility>

using namespace std;

int main() {
    freopen("bills.in", "r", stdin);
    vector<pair<string, int> > customers;

    int N;
    cin >> N;
    for (int i = 1; i < N; i++) {
        string name;
        int cost;
        cin >> name >> cost;
        customers.push_back(make_pair(name, cost));
        cout << customers[i].second << " ";
    }
    return 0;
}
#include<iostream>
using namespace std;
int main() {
	int N, mul = 1, d;
	cout <<" Creates a multiple of positive integers of a number." << endl;
	cout << "Enter number: ";
	cin >> N;
	while (N > 0) {
		d = N % 10;
		if (d % 2 ==0) {
			mul = mul * d;
			}
		N = N / 10;
		}
	cout << " Multiple is: " << " " << mul << endl;
}

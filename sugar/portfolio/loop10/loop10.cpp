#include<iostream>
using namespace std;
int main() {
	int N, max = 0, d;
	cout << "Get the largest integer from a number. Enter: " << endl;
	cin >> N;
	while ( N > 0) {
		d = N % 10;
		N = N / 10;
		if ( max < d ) {
			max = d;
		}
	}
	cout << "Largest integer is: " <<" " << max << endl;
}

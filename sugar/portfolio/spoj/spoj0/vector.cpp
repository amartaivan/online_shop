#include<iostream>

using namespace std;

int main() {
    freopen("test.txt", "r", stdin);
    srand(time(NULL));
    int N;
    cin >> N;
    int a[N];
    for (int i = 0; i < N ; i ++) {
        cin >> a[i];
    }
    int max_val = a[0];
    for (int i = 1; i < N; i++ ) {
        max_val = max(max_val, a[i]);
    }
    cout << max_val << endl;
    return 0;
}
#include<iostream>

using namespace std;

int main() {
    int day, hour, input;
    cin >> input;

    day = input / 24;
    hour = input % 24;

    cout << day << " " << hour;
    return 0;
}
#include<iostream>
using namespace std;

string decimaltobinary( int N ) {
    string answer = string();
    while(N > 0) {
        if(N % 2 == 0) 
            answer.push_back('0');
        else 
            answer.push_back('1');
        N = N / 2;
    }

    reverse(begin(answer), end(answer));
    return answer; 
}

int main() {
   int num;
   cout << "Enter number: ";
   cin >> num;
   cout << decimaltobinary(num) << endl;
}
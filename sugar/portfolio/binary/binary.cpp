#include<iostream>
using namespace std;

int binarytodecimal(string temp) {
    int decimal = 0;
    int len = temp.size();
    int prod = 1;
    for (int i = len - 1; i >=0; i--){
        if (temp[i] == '1')
            decimal = decimal + prod;
        prod = prod * 2;
    }
    return decimal;
}
int main() {
    string input;
    cout <<"Enter Binary: ";
    cin >> input;
    cout << binarytodecimal(input) << endl;

}
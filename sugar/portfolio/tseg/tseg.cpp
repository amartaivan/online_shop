#include<iostream>
#include<cmath>
#include<vector>

using namespace std;

double dis( double x, double y, double a, double b) {
    return sqrt(pow(x-a,2) + pow(y-b,2));
}

int main() {
    double x, y, a, b, answer;
    cin >> x >> y >> a >> b;
    answer = dis(x,y,a,b);
    cout << answer << endl; 
}
#include<iostream>
#include<string>
#include<fstream> 

using namespace std;

int main() {
    ifstream in("todo.txt");

    string prod;
    string job[100];

    int n = 0, i = 0, cmd;
    bool temp = true;
    
    while(getline(in,prod)) {
        job[n] = prod;
        n++;
    }
    
    while (temp) {
        cout << "Press 0 for listing all jobs." << endl;
        cout << "Press 1 for adding a new job." << endl;
        cout << "Press 9 for exit." << endl;
        cin >> cmd;
        switch (cmd) {
            case 0: {
                int i = 0;
                while (i < n) {
                    cout << i + 1 << ". " << job[i] << endl;
                    i = i + 1;
                }
                break;
            }
            case 1: {
                string new_job;
                cin >> new_job;
                job[n] = new_job;
                ofstream out("todo.txt", ios::app);
                n = n + 1; 
                break;
            }
            case 9: {
                temp = false;
                break;
            }
        }
    }
 

    return 0;
}
#include<iostream>
#include<vector>
using namespace std;

pair<int, int> up(pair<int, int> input){
    input.second++;
    return input;
}
pair<int, int> down(pair<int, int> input){
    input.second--;
    return input;
}
pair<int, int> right(pair<int, int> input){
    input.first++;
    return input;
}
pair<int, int> left(pair<int, int> input){
    input.first--;
    return input;
}


int main() {
    vector<pair<int, int>> p(2);


    cout << "Enter two coordinates: " << endl;
    cin >> p[0].first >> p[0].second; 
    cin >> p[1].first >> p[1].second;

    char u,d, r, l;
    int N;
    cout << "Enter how many commands: ";
    cin >> N;
    for ( int i = 0; i < N; i++) {
        int a;
        char b = 'u';
        cin >> a >> b;
        a--;
        switch (b) {
            case 'u' :
                p[a] = up(p[a]); 
                break;
            case 'd' :
                p[a] = down(p[a]);
                break;
            case 'r' :
                p[a] = right(p[a]);
                break;
            case 'l':
                p[a] = left(p[a]);
                break;
        }
        
    }
    cout << "New coordinate for p1 is: " << p[0].x << " " << p[0].y << endl;
    cout << "New coordinate for p2 is: " << p[1].x << " " << p[1].y << endl;

}

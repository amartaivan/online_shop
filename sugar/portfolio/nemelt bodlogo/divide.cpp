#include<iostream> 
#include<vector>

using namespace std;

int hieh(int x, int y){
    while (x != 0 && y != 0){
                if(x > y)
                    x = x % y;
                else 
                    y = y % x;
            }
            return x + y;            
        }

struct Fraction {
    int num, den;

    void print() {
        simplify();
        cout << num << "/" << den << endl;
    }

    void simplify() {
        int c = hieh(num, den);
        num /= c;
        den /= c;
    }

    void add(Fraction second) {
        num = num * second.den + second.num * den;
        den = den * second.den;
    }

    void minus(Fraction second) {
        num = num * second.den - second.num * den;
        den = den * second.den;
    }
};

Fraction add(Fraction a, Fraction b) {
    Fraction ans;
    ans.num = a.num * b.den + b.num * a.den;
    ans.den = a.den * b.den;
    return ans;
}


int main() {
    Fraction too1, too2;
    cin >> too1.num >> too1.den >> too2.num >> too2.den;
    char character = '+';
    cin >> character;
    switch (character) {
        case '+':
            too1.add(too2);
        case '-':
            too1.minus(too2);
         }
    too1.print();
    return 0;
}
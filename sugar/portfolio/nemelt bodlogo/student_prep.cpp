#include<iostream>
#include<cmath>
#include<vector>
#include<string>

using namespace std;

struct Exam {
    double exam1, exam2, final_exam;
};

struct Student {
    string name, sex, address;
    int age, grade;
    Exam ex;
   
    Student (string _name, int _age, string _sex, int _grade, string _address) {
        name = _name;
        age = _age;
        sex = _sex;
        grade = _grade;
        address = _address;
    }
};

int main() {
    string name, sex, address;
    int age, grade;
    vector <Student> student_list;
    int selection = -1;
    while (selection !=0) {
        cout <<"--Add New Student-- (enter 1)" << endl;
        cout <<"--Edit Student Info-- (enter 2)" << endl;
        cout <<"--List All Student-- (enter 3)" << endl;
        cout <<"--Enter Exam Score-- (enter 4)" << endl;
        cout <<"--Show Student Detail-- (enter 5)" << endl;
        cout <<"--Exit from app-- (enter 0)" << endl;
        cin >> selection;
        switch(selection) {
            case 1: {
                cout <<"Enter Name: ";
                cin >> name;
                cout << endl;
                cout <<"Enter Age: ";
                cin >> age;
                cout << endl;
                cout << "Enter Sex: ";
                cin >> sex;
                cout << endl;
                cout <<"Enter Grade: ";
                cin >> grade;
                cout << endl;
                cout <<"Enter Address: ";
                cin >> address;
                cout << endl;
                Student temp = Student(name, age, sex, grade, address);
                student_list.push_back(temp);
                break;
            }
            case 3: {
                for(int i = 0; i < student_list.size(); i++) {
                    cout << i + 1 << " " << student_list[i].name << endl;
                }
                break;
            }
            case 5:
                cout << "Enter Student Name: ";
                cin >> name;
        }
    }
}
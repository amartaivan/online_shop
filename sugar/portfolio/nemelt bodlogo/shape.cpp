#include<iostream>
#include<vector>
#include<cmath>


using namespace std;

const double pi = 3.14;

struct Shape {
    float a, b, c, area;

    void circle() {
        area = pi * a * a;
    }

    void square() {
        area = a * b;
    }

    void triangle() {
        int s = (a + b +c) /2;
        area = sqrt(s * (s - a)*(s - b)*(s - c));
    }
};


int main() {
    int x;
    cout << "Enter how many shapes: ";
    cin >> x;
    vector <Shape> sh(x);
    for(int i = 0; i < x; i ++) {
        int input;
        cout << "Enter your shape number: ";
        cin >> input;
        switch(input) {
        case 1:
            cout <<"Enter circle radius: ";
            cin >> sh[i].a;
            sh[i].circle();
            cout <<"Area is: " << sh[i].area << endl;
            break;
        case 2:
            cout <<"Enter two square sides: ";
            cin >> sh[i].a >> sh[i].b;
            sh[i].square();
            cout <<"Area is: " << sh[i].area << endl;
            break;
        case 3:
            cout <<"Enter three triangle sides: ";
            cin >> sh[i].a >> sh[i].b >> sh[i].c;
            sh[i].triangle();
            cout << "Area is: " << sh[i].area << endl;
            break;
        }
    }
    int i;
    cout << "Enter n th entered number: " ;
    cin >> i;
    cout << i << "th shape's are is " << sh[i-1].area << endl;
}
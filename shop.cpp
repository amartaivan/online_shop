#include<iostream>
#include<vector>

using namespace std;

double price = 46;

double compare(double a, double b){
    if(a > b)
        return 1;
    else
        return 0;
}

double sale(double kilo){
    double bill = kilo * price;
    if(compare(bill, 200) == 1){
        bill *= 0.8;
    }else
        bill *= 0.9;
        return bill;
}

int main(){

    vector <pair <string, double> > customers;
    double kg;
    string name;


    for(int j = 1; j <=7; j++){
        switch(j) {
            case 1 :
                cout << "Today is monday" << endl;
                break;
            case 2 :
                cout << "Today is tuesday" << endl;
                break;
            case 3 :
                cout << "Today is wednesday" << endl;
                break;
            case 4 :
                cout << "Today is thursday" << endl;
                break;
            case 5 :
                cout << "Today is friday" << endl;
                break;
            case 6 :
                cout << "Today is saturday" << endl;
                break;
            case 7 :
                cout << "Today is sunday" << endl;
                break;
        }
        while(true){
            cout << "Enter your name " << endl;
            cin >> name;
            cout << "Enter your amount" << endl;
            cin >> kg;
            if (kg == 0)
                break;
            customers.push_back(make_pair(name, sale(kg)));
        }

    }
    return 0;
}
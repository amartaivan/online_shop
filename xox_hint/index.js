let buttons = document.getElementsByClassName('cell');
let buttonsState = [-1, -1, -1, -1, -1, -1, -1, -1, -1];
let replayButton = document.getElementById('replay-button');
let somethingButton = document.getElementById('something');
let gameOverMsg = document.getElementsByClassName('game-over-msg')[0];
let turn = 0;
let gameOver = false;
let winner;
let winningCombinations = [
    [0, 1, 2],
    [3, 4, 5],
    [6, 7, 8],
    [0, 4, 8],
    [2, 4, 6],
    [0, 3, 6],
    [1, 4, 7],
    [2, 5, 8]
];

for (let i = 0; i < buttons.length; i++) {
    buttons[i].addEventListener('click', function buttonClicked() {
        if (buttonsState[i] === -1 && !gameOver) {
            turn = (turn + 1) % 2;
            buttonsState[i] = turn;
            if (turn % 2 === 1) {
                buttons[i].style.backgroundColor = 'red';
            } else {
                buttons[i].style.backgroundColor = 'rgba(56, 126, 169)';
            }
        }

        isGameOver();
    })
}

function isGameOver() {
    for (let i = 0; i < winningCombinations.length; i++) {
        if (buttonsState[winningCombinations[i][0]] !== -1
            && buttonsState[winningCombinations[i][0]] === buttonsState[winningCombinations[i][1]] 
            && buttonsState[winningCombinations[i][0]] === buttonsState[winningCombinations[i][2]]) {
            gameOver = true;
            gameOverMsg.innerHTML = 'Player ' + Number(buttonsState[winningCombinations[i][0]] + 1) + ' WINS!';
            gameOverMsg.style.display = 'block';
        }
    }
}

replayButton.addEventListener('click', replay);

function replay() {
    gameOver = false;
    turn = 0;
    buttonsState = [-1, -1, -1, -1, -1, -1, -1, -1, -1];
    gameOverMsg.style.display = 'none';
    for (let i = 0; i < buttons.length; i++) {
        buttons[i].style.backgroundColor = 'white';
    }
}

somethingButton.addEventListener('click', () => {
    gameOverMsg.innerHTML = 'VOTE FOR LOUIS';
    gameOverMsg.style.display = 'block';
});

// HUUHDUUDEE, OORSDOO 2 TOGLOGCH TENTSSEN TOHIOLDOLD GARAH MSG-IIG SOLIOROI.
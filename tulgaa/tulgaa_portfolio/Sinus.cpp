#include<iostream>
#include<cmath>
#include<iomanip>
using namespace std;

int main() {
    double x, sum = 0, resu; 
    int n;
    cin >> x >> n;
    for(int i = 1; i <= n; i++) {
        resu = sin(pow(x, i));
        sum += resu;
    }  
    cout <<fixed << setprecision(3) << sum << endl;
}
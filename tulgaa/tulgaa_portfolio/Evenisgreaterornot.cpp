#include<iostream>
#include<iomanip>
using namespace std;

int main() {
    int n, e = 0, o = 0;
    cin >> n;
    int num[n];
    for(int i = 0; i < n; i++) 
        cin >> num[i];
    for(int i = 0; i < n; i++){ 
        if(num[i] % 2 == 0) {
            e += 1;
        }else{
            o += 1;
        }
    }    
    if(o > e) {
        cout << "YES" << endl;
    }else{
        cout << "NO"  << endl;
    }
    return 0;
}
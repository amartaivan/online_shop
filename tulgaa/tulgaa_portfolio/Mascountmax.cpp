#include<iostream>
#include<iomanip>
using namespace std;

int main() {
    int n, max, count = 0;
    cin >> n;
    int mas[n];
    for(int i = 0; i < n; i++) 
        cin >> mas[i];
        max = mas[0];
    
    for(int i = 0; i < n; i++){ 
        if(max < mas[i]) 
            max = mas[i];
    }
    for(int i = 0; i < n; i++){    
        if(max == mas[i]) 
            count += 1;

    }
    cout << max << " " << count << endl;
    return 0; 
}

#include<iostream>
#include<iomanip>
using namespace std;

int main() {
    int n, sum = 0;
    cin >> n;
    for(int i = 1; i <= n; i = i + 2) {
        sum = sum + i * ( i + 1);
    }
    cout << sum << endl;
    return 0; 
}

#include<iostream>
#include<cmath>
#include<vector>

using namespace std;

struct Shape {
    double a;

    void cir() {
        double p = 3.14;
        a = a * a * p; 
    }

    void rec(Shape second) {
        a = a * second.a;
    }

    void tri(Shape second, Shape third) {
        double s;
        s = (a + second.a + third.a);
        a = sqrt(s*(s-a)*(s-second.a)*(s-third.a));
    }
};


int main() {
    int j;
    cin >> j;
    vector<Shape> m[j];
    Shape x;
    for(int i = 0; i < j; i++) {
        int n;
        double w;
        cin >> n;
        switch (n) {
        case 1: cin >> x.a;
            m[i] = x.cir();
            break;
        case 2: cin >> x.a >> x.b; 
            x.rec();
            m.push_back(x);
            break;
        case 3: cin >> x.a>> x.b >> x.c;
            x.tri();
            m.push_back(x);
            break;
        default:
            break;
        }
    }
   
    
    
    int b;
    cin >> b;
    return 0;
}
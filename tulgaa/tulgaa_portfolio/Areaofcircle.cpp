#include<iostream>
#include<iomanip>
using namespace std;

int main() {
    double l, r, pi = 3.141592;
    cin >> l;
    r = l / (2 * pi);
    cout << fixed << setprecision(4) << r * r * pi << endl;
    return 0;
}
#include<iostream>
#include<iomanip>
using namespace std;

int main() {
    int n, e, fac = 1;
    cin >> n;
    e = n / 2;
    if(n % 2 == 0) {
        for(int i = 2; i <= n; i += 2 ) 
            fac *= i;
    }else{
        for(int i = 1; i <= n; i += 2 ) 
            fac *= i;
    }
    cout << fac << endl;
    return 0;
    }
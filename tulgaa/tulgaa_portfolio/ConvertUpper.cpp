#include<iostream>
#include<cmath>
#include<iomanip>
using namespace std;

int main() {
    char a;
    for(int i = 0; i < 3; i++) {
        cin >> a;
        if(int(a) >= 97) {
            cout << char(a - 32) << " ";
        }else{
            cout << a << " ";
        }
    }
    cout << endl;
    return 0; 
}

#include<iostream>
#include<cmath>
#include<vector>
#include<struct.h>

using namespace std;

const double pi = 3.14;

struct Shape {
    double a, b, c;
    double area;

    void cir() {
        area = a * a * pi; 
    }

    void rec() {
        area = a * b;
    }

    void tri() {
        double s;
        s = (a + b + c);
        area = sqrt(s*(s-a)*(s-b)*(s-c));
    }
};

struct Shape2 {
    double a, b, c;
    double area;

    Shape2(int _a) {
        a = _a;
        area = a * a * pi;
    }

    Shape2(int _a, int _b) {
        a = _a;
        b = _b;
        area = a * b;
    }

    Shape2(int _a, int _b, int _c) {
        a = _a;
        b = _b;
        c = _c;
        double s;
        s = (a + b + c);
        area = sqrt(s*(s-a)*(s-b)*(s-c));
    }
};


int main() {
    int j;
    int a, b, c;
    cin >> j;
    vector<Shape2> shapes(j);

    for(int i = 0; i < j; i++) {
        int n;
        cin >> n;
        switch (n) {
        case 1: 
            cin >> a;
            shapes[i].a = new Shape2(a);
                
            break;
        case 2: 
            cin >> a >> b; 
            shapes[i] = new Shape2(a, b);
            break;
        case 3: 
            cin >>a >> b >> c;
            shapes[i] = new Shape2(a, b ,c);
            break;
        default:
            break;
        }
    }
   
    int m, b;
    cin >> m;
    for(int i = 0; i < m; i++) {
        cin >> b;
        cout << "Area of shape " << b << " is " << shapes[b - 1].area << endl;
    }
    return 0;
}
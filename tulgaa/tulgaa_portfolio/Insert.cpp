#include<iostream>
#include<fstream>
#include<vector>
#include<ctime>
 
using namespace std;

vector<int> insertion_sort(vector<int> arr){
    int n = arr.size();
    for(int split = 0; split < n + 1; split++) {
        int head = arr[split + 1];
        int not_lower_idx = 0;
        while(not_lower_idx < split && arr[not_lower_idx] < head) {
            not_lower_idx++;
        }
        for(int j = split; j >= not_lower_idx; j--) {
            arr[j + 1] = arr[j];
        }
        arr[not_lower_idx] = head;

        }       

    return arr;
}
int main() {
    ifstream input("test.txt");
    int n;
    input >> n;
    vector<int>arr[n], sorted_by_insertion;

    for(int i = 0; i < n; i++)
        input >> arr[i];

    clock_t begin,  end;
    begin = clock();
    sorted_by_insertion = insertion_sort(arr);
    end = clock();
    ofstream insert_out("insert.out");
    insert_out << sorted_by_insertion.size() << endl;
    for(int i = 0; i < sorted_by_insertion.size(); i++)
        insert_out << sorted_by_insertion[i] << " ";
    insert_out << endl;
    cout << "insert sort time : " << double ( ) ;
    return 0;
}
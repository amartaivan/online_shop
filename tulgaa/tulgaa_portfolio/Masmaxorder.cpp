#include<iostream>

using namespace std;

int main() {
    int n, max, count = 0;
    cin >> n;
    int num[n];
    for(int i = 1; i <= n; i++) {
        cin >> num[i];
    }
    max = num[1];
    for(int i = 1; i <= n; i++) {
        if(max < num[i]) {
            max = num[i];
            count = i;
        }
    }

    cout << max << " " << count << endl;
    return 0;
}
#include<iostream>
using namespace std;

int to_bin(int dec) {
    int bin = 0, rem;
    while(dec > 0) {
        rem = dec % 2;
        bin = bin * 10 + rem;
        dec = dec / 2;
    }
    return bin;
}

int main() {
    int n, ans;
    cin >> n;
    ans = to_bin(n);

    cout << ans << endl;
    return 0;
}
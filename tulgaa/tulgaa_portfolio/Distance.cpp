#include<iostream>
#include<cmath>
#include<vector>
using namespace std;

double distwo(pair<int, int> a, pair<int, int> b){
    return sqrt(pow(a.first - b.first, 2) + pow(a.second - b.second, 2));
}

double Dis(vector<pair<int, int> > points) {

    double result;
    points.push_back(points[0]);
    for (int i = 1; i < points.size(); i++){
        result += distwo(points[i], points[i - 1]);
    }

    return result;
}

int main() {

    int n;
    cin >> n;
    vector<pair<int, int> > points(n);
    for (int i = 0; i < n; i++)
        cin >> points[i].first >> points[i].second;

    double result = Dis(points);

    cout << result << endl;

    return 0;
}



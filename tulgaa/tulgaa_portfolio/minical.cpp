#include<iostream>

using namespace std;
 
int hieh(int x, int y) {
     while(x > 0 && y > 0) {
         if(x > y) x = x % y;
         else y = y % x;
     }
     return x + y;
 }

struct Cal{
    int a, b;

    void simplify() {
        int h = hieh(a, b);
        a /= h;
        b /= h;
    }

    void printFraction() {
        simplify();
        cout << a << "/" << b << endl;
    }

    void add(Cal second) {
        a = a * second.b + second.a * b;
        b = b * second.b; 
    }

};

Cal add2(Cal x, Cal y) {
    x.add(y);
    return x;
}

Cal add(Cal x, Cal y) {
    Cal temp;
    temp.a = (x.a * y.b) + (y.a * x.b);
    temp.b = x.b * y.b;
    return temp;
}

Cal sub(Cal x, Cal y) {
    Cal temp;
    temp.a = (x.a * y.b) - (y.a * x.b);
    temp.b = x.b * y.b;
    return temp;
}

Cal mult(Cal x, Cal y) {
    Cal temp;
    temp.a = (x.a * y.a);
    temp.b = x.b * y.b;
    return temp;
}

Cal dev(Cal x, Cal y) {
    Cal temp;
    temp.a = x.a * y.b;
    temp.b = x.b * y.a;
    return temp;
}

int main() {
    Cal x, y;

        cin >> x.a >> x.b >> y.a >> y.b;
        x.add(y);
        char e;
        cin >> e;
        Cal ans;
        // ans.a = 84;
        // ans.b = 24;
        // ans.printFraction();
        switch (e) {
        case '+': ans = add(x, y);
            break;
        case '-': ans = sub(x, y);
            break;
        case '*': ans = mult(x, y);
            break;
        case '/': ans = dev(x, y);
            break;
        }
        ans.printFraction();
    return 0;
}

#include<iostream>
#include<cmath>
#include<vector>
#include<struct.h>

using namespace std;

const double pi = 3.14;

struct Shape {
    double a, b, c;
    double area;

    void cir() {
        area = a * a * pi; 
    }

    void rec() {
        area = a * b;
    }

    void tri() {
        double s;
        s = (a + b + c);
        area = sqrt(s*(s-a)*(s-b)*(s-c));
    }
};


int main() {
    int j;
    cin >> j;
    vector<Shape> shapes(j);

    for(int i = 0; i < j; i++) {
        int n;
        cin >> n;
        switch (n) {
        case 1: 
            cin >> shapes[i].a;
            shapes[i].cir();
            break;
        case 2: cin >> shapes[i].a >> shapes[i].b; 
            shapes[i].rec();
            break;
        case 3: cin >> shapes[i].a>> shapes[i].b >> shapes[i].c;
            shapes[i].tri();
            break;
        default:
            break;
        }
    }
   
    int m, b;
    cin >> m;
    for(int i = 0; i < b; i++) {
        cin >> b;
        cout << "Area of shape " << b << " is " << shapes[b - 1].area << endl;
    }
    return 0;
}
#include<iostream>
#include<cmath>
#include<iomanip>
using namespace std;

int main() {
    int a, b, c;
    cin >> a >> b >> c; 
    if((c * c == a * a + b * b )|| (b * b == a * a + c * c) || (a * a == c * c + b * b) ) {
        cout << "Right" << endl;
    }else{
        if((a * a + b *b > c *c) && (a * a + c * c > b * b) && (b * b + c * c > a * a)) {
            cout << "Acute" << endl;
        } else {
            cout << "Obtuse";
        }
    }
    return 0; 
}

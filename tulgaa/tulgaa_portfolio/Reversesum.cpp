#include<iostream>
#include<vector>
#include<cmath>
#include<iomanip>

using namespace std;
int main() {
    int n;
    double a, mul = 1;
    cin >> n;
    for(int i = 1; i <= n; i++) {
        a = (1 + 1/(pow(i,2)));
        mul *= a;
    }
    cout << fixed << setprecision(3) << mul << endl;
}
       
      
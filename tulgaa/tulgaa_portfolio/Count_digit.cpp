#include<iostream>
#include<cmath>
#include<iomanip>
using namespace std;

int main() {
    long long n, count = 0;
    cin >> n;
    if(n == 0){
        cout << 1 <<endl;
    }else{   
        while(n > 0) {
            n = n / 10;
            count += 1;
        }
        cout << count << endl;
    }
    return 0; 
}

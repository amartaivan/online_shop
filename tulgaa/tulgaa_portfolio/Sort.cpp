#include<iostream>
#include<fstream>
#include<vector>
#include<ctime>

using namespace std;

vector<int> selection_sort(vector<int> vecsort){
    int r = vecsort.size();;
    int heden_udaa = vecsort.size();;
    
    for(int sortloh_step = 0; sortloh_step < heden_udaa; sortloh_step++) {
        int big_index = 0;
        for (int j = 0; j < r; j++)    {
            if (vecsort[j] > vecsort[big_index]){
                big_index = j;
            }
        }
        r--;
        swap(vecsort[big_index], vecsort[r]);
    }

    return vecsort;
}
vector<int> bubble_sort(vector<int> vecsort){
    int temp;
    for(int i = 0; i < vecsort.size(); i++) {
        for(int j = i + 1; j < vecsort.size();j++) {
            if(vecsort[i] > vecsort[j]) {
                temp = vecsort[j];
                vecsort[j] = vecsort[i];
                vecsort[i] = temp;
            }
        }
    }


    return vecsort;
}

int main() {

    clock_t begin, end;

    freopen("Ntoo.txt", "r", stdin);

    int n;
    cin >> n;
    vector<int> vecsort(n), sorted_by_select, sorted_by_bubble;
    for(int i = 0; i < n; i++) {
        cin >> vecsort[i];
    }

    begin = clock();
    sorted_by_select = selection_sort(vecsort);
    end = clock();
    ofstream select_out("selection.out");

    for(int i = 0; i < n; i++) 
        select_out << sorted_by_select[i] << " ";
    select_out << endl;
    cout << "selection_sort time :  " << double(end - begin) / 1000000 << endl;

    begin = clock();
    sorted_by_bubble = bubble_sort(vecsort);
    end = clock();
    ofstream bubble_out("Buble.out");

    for(int i = 0; i < n; i++) 
        bubble_out << sorted_by_bubble[i] << " ";
    bubble_out << endl;
    cout << "bubble_sort time :  " << double(end - begin) / 1000000 << endl;


    return 0;
}

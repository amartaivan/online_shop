#include<iostream>
using namespace std;
int main(){
        int n, mult = 1, remainder;
        cout << "Enter number: ";
        cin >> n;
        while(n > 0){
                remainder = n % 10;
                if(remainder % 2 == 0){
                        mult = mult * remainder;
                        }
                n = n / 10;
                }
        cout << "Multiple of even digits = "<< mult << endl;
        return 0;
}


#include <iostream> 
#include<cmath>
using namespace std; 
int main() { 
    int d, o , h, n, orts, dav, hal, rem;
    cin >> d >> o >> h;
    cin >> n;
    rem = n % ( d * h);
    if(rem == 0){
        orts = n / (d * h);
        dav = d;
        hal = h;
    }else{
        orts = n / (d * h) + 1;
        if( rem % h == 0 ){
            dav = rem / h;
            hal = h;
        }else{
            dav = rem / h + 1;
            hal = rem % h;
        }
    }
    cout << orts << " " << dav << " " << hal << endl;
    }
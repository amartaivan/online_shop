#include <fstream>
#include <iostream>
using namespace std;

int main() {
    int n = 0, a; 
    string job[100];
    bool exit = false;
    while(exit == false) {
        cout << "Press 0 for listing all jobs" << endl;
        cout << "Press 1 for add new jobs" << endl;
        cout << "Press 9 for exit program" << endl;
        cin >> a;
        switch(a) {
            case 0 : {
                for(int i = 0; i < n; i++) 
                    cout << i + 1 << ". " << job[i] << endl;
                break;
            }
            case 1 : {
                string new_job;
                cin >> new_job;
                job[n] = new_job;
                n++;
                break;
            }
            case 9 : {
                exit = true;
                break;
            }        
        }   
    }
    return 0;
}
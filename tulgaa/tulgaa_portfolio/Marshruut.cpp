#include<iostream>
#include<cmath>
#include<iomanip>
using namespace std;

int main() {
    int n;
    cin >> n;
    long long fib[n];
    fib[0] = 0;
    fib[1] = 1;
    fib[2] = 2;
    for(int i = 3; i <= n; i++) {
        fib[i] = fib[i - 1] + fib[i - 2];
        // if(fib[i] == 0)
        //     fib[i] = i;
    }
    cout << fib[n];
    }
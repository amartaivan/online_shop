#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;


int main() {
    /* Enter your code here. Read input from STDIN. Print output to STDOUT */   
    int n, m;
    cin >> n >> m;
    if(n > 0) {
        if(m < 100) {
            for(int i = n - 1; i > 0; i--) {
                for(int j = m - 1; j > 0; j--) {
                    if((i + j) % 2 == 0) {
                        cout << "#";
                    }else{
                        cout << "_";
                    }    
                }
                cout << "\n";
            }
        }
    }
    return 0;
}